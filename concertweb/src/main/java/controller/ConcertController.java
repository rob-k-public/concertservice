package controller;

import dto.ConcertDto;
import entity.Concert;
import facade.ConcertFacade;
import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

@Path("/concert")
public class ConcertController {

    @Inject
    private ConcertFacade facade;

    @GET
    @Path("/health")
    public String getHealth() {
        return "Service is running.";
    }

    @GET
    @Path("/all")
    public List<Concert> getAllConcerts() {
        return facade.getAllConcerts();
    }

    @POST
    @Path("/add")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ConcertDto addConcert(ConcertDto concert) {
        facade.addConcert(concert);
        return concert;
    }

    @GET
    @Path("/find/{id}")
    public Concert getById(@QueryParam("id")long id) {
        return facade.getConcert(1L);
    }
}

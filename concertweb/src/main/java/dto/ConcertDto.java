package dto;

import java.util.Date;

public class ConcertDto {
    
    private PlaceDto place;
    private String artist;
    private Date date;

    public ConcertDto() {
    }

    public PlaceDto getPlace() {
        return place;
    }

    public void setPlace(PlaceDto place) {
        this.place = place;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;   
   } 
}

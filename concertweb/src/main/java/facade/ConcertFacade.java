package facade;

import dto.ConcertDto;
import entity.Concert;

import java.util.List;

public interface ConcertFacade {

    void addConcert(ConcertDto concertdto);
    Concert getConcert(long id);
    List<Concert> getAllConcerts();
    String getDbType();
}

package facade;

import dto.ConcertDto;
import entity.Concert;
import entity.Place;
import service.ConcertService;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Default;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;

@Named
@RequestScoped
@Default
public class ConcertFacadeImpl implements ConcertFacade {

    @Inject
    private ConcertService service;

    @Override
    public void addConcert(ConcertDto concertdto) {
        Place place = new Place(concertdto.getPlace().getName(), concertdto.getPlace().getCity(), concertdto.getPlace().getCountry());
        Concert concert = new Concert(concertdto.getArtist(), place, concertdto.getDate());
        service.addConcert(concert);
    }

    @Override
    public Concert getConcert(long id) {
        return service.getConcert(id);
    }

    @Override
    public List<Concert> getAllConcerts() {
        return service.getAllConcerts();
    }

    @Override
    public String getDbType() {
        return service.getDbType();
    }
}

package entity;

import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Concert {

    @GeneratedValue @Id
    private long id;
    private String artist;
    @OneToOne(targetEntity = Place.class, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Place place;
    @Temporal(TemporalType.DATE)
    private Date date;

    public Concert() {
    }

    public Concert(String artist, Place place, Date date) {
        this.artist = artist;
        this.place = place;
        this.date = date;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public Place getPlace() {
        return place;
    }

    public void setPlace(Place place) {
        this.place = place;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}

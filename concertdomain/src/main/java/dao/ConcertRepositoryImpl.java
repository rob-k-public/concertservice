package dao;

import entity.Concert;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Default;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.util.List;

@Named
@RequestScoped
@Default
public class ConcertRepositoryImpl implements ConcertRepository {

    @PersistenceContext(unitName = "ConcertPUnit")
    EntityManager em;

    private String dbType = "JPA";

    @Override
    @Transactional
    public void addConcert(Concert concert) {
        em.persist(concert);
    }

    @Override
    public Concert getConcert(long id) {
        return em.find(Concert.class, id);
    }

    @Override
    public List<Concert> getAllConcerts() {
        Query query = em.createQuery("SELECT c FROM Concert c");
        return (List<Concert>) query.getResultList();
    }

    @Override
    public String getDbType() {
        return dbType;
    }

}

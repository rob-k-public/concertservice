package dao;

import entity.Concert;

import java.util.List;

public interface ConcertRepository {

    void addConcert(Concert concert);
    Concert getConcert(long id);
    List<Concert> getAllConcerts();
    String getDbType();
}

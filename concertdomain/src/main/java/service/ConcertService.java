package service;

import entity.Concert;

import java.util.List;

public interface ConcertService {

    void addConcert(Concert concert);
    Concert getConcert(long id);
    List<Concert> getAllConcerts();
    String getDbType();
}

package service;

import dao.ConcertRepository;
import entity.Concert;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Default;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;

@Named
@RequestScoped
@Default
public class ConcertServiceImpl implements ConcertService {

    @Inject
    private ConcertRepository repository;

    @Override
    public void addConcert(Concert concert) {
        repository.addConcert(concert);
    }

    @Override
    public Concert getConcert(long id) {
        return repository.getConcert(id);
    }

    @Override
    public List<Concert> getAllConcerts() {
        return repository.getAllConcerts();
    }

    @Override
    public String getDbType() {
        return repository.getDbType();
    }
}
